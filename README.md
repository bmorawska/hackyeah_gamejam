# O projekcie / About project

<img src="https://static.dwcdn.net/css/flag-icons/flags/4x3/pl.svg" height="10" width="20">
Projekt gry komputerowej realizowany jest w ramach konkursu HackYeah Powered by Govtech dla kategorii Game Jam.


Uczestnicy gwarantują, że przedstawione przez nich projekty gier komputerowych są ich autorstwa lub współautorstwa (w przypadku pracy zespołowej) i nie naruszają jakichkolwiek praw osób trzecich. 

<img src="https://static.dwcdn.net/css/flag-icons/flags/4x3/gb.svg" height="10" width="20"> 
The computer game project is part of the HackYeah Powered by Govtech competition for Game Jam category.


Participants guarantee that they are the authors or co-authors (in the case of teamwork) of the computer game designs (hereinafter referred to as the Work) presented by them and that these designs do not violate any third party rights. 

# Autorzy / Authors
- Paweł Lewandowski
- Barbara Morawska
- Andrzej Sasinowski
- Filip Turoboś

# Wykorzystana technologia / Used technology

<img src="https://static.dwcdn.net/css/flag-icons/flags/4x3/pl.svg" height="10" width="20">
Projekt został napisany przy pomocy silnika Unity w wersji 2019.4.10f1.  

<br/>

<img src="https://static.dwcdn.net/css/flag-icons/flags/4x3/gb.svg" height="10" width="20"> 
The project was coded with the Unity engine version 2019.4.10f1.