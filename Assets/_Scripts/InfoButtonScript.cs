﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoButtonScript : MonoBehaviour
{
    public Image infoImg;
    public void InfoButton()
    {
        infoImg.gameObject.SetActive(!infoImg.gameObject.activeSelf);
    }
}
