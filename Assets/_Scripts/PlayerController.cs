﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public int FieldColumnsNumber = 10;
    public int FieldRowsNumber = 10;

    public GameObject[,] Field;
    public GameObject sadGirlsAndBoys;

    public Sprite GirlSprite;
    public Sprite BoySprite;

    public Text scoreText;
    public Text sadBoysText;
    public Text sadGirlsText;

    public Image YouWin;
    public Image YouLost;

    public Button RestartButton;
    public Button QuitButton;

    public GameObject GameGrid;

    public int movesMade = 0;
    public int sadGirlsCounter = 100;
    public int sadBoysCounter = 100;

    public bool cannotContinue = false;
    public bool isWin = true;

    public GameObject BoyPrefab;
    public GameObject GirlPrefab;

    public RuntimeAnimatorController girlController;
    public RuntimeAnimatorController boyController;

    // Start is called before the first frame update
    void Start()
    {
        this.Field = new GameObject[FieldRowsNumber, FieldColumnsNumber];

        int children = transform.childCount;
        for (int i = 0; i < children; ++i)
        {
            GameObject child = transform.GetChild(i).gameObject;
            int idxX = (int) (4.5 - child.transform.position.y);
            int idxY = (int) (child.transform.position.x + 4.5);
            this.Field[idxX, idxY] = child;
        }
        scoreText.text = "0";
        CountBorders();
    }

    public void FertilizeChild()
    {
        bool hasBeenFertilized = false;

        int counter = 0;
        for (int i = 0; i < FieldRowsNumber; i++)
        {
            for (int j = 0; j < FieldColumnsNumber; j++)
            {
                if (Field[i, j].tag != "Empty")
                    counter++;
            }
        }

        if (counter < (FieldColumnsNumber - 2) * (FieldRowsNumber - 2))
        {
            while (!hasBeenFertilized)
            {
                int newChildColumn = Random.Range(1, FieldColumnsNumber - 1);
                int newChildRow = Random.Range(1, FieldRowsNumber - 1);
                if (Field[newChildRow, newChildColumn].tag == "Empty")
                {
                    hasBeenFertilized = true;
                    int girlOrBoy = Random.Range(0, 2);
                    if (girlOrBoy == 0)
                    {
                        Field[newChildRow, newChildColumn].tag = "Girl";
                        Field[newChildRow, newChildColumn].transform.GetComponent<SpriteRenderer>().sprite = GirlSprite;
                        Field[newChildRow, newChildColumn].AddComponent<Animator>();
                        Field[newChildRow, newChildColumn].GetComponent<Animator>().runtimeAnimatorController = girlController;
                        Field[newChildRow, newChildColumn].AddComponent<KafelekGirlController>();
                    }
                    else
                    {
                        Field[newChildRow, newChildColumn].tag = "Boy";
                        Field[newChildRow, newChildColumn].transform.GetComponent<SpriteRenderer>().sprite = BoySprite;
                        Field[newChildRow, newChildColumn].AddComponent<Animator>();
                        Field[newChildRow, newChildColumn].GetComponent<Animator>().runtimeAnimatorController = boyController;
                        Field[newChildRow, newChildColumn].AddComponent<KafelekBoyController>();
                    }
                }
            }
        }
        else
        {
            cannotContinue = true;
            isWin = false;
        }

    }

    private void Swap(int r, int c, int row, int column)
    {
        Vector3 temp = Field[r, c].transform.position;
        Field[r, c].transform.position = Field[row, column].transform.position;
        Field[row, column].transform.position = temp;
        
        GameObject tempGO = Field[r, c];
        Field[r, c] = Field[row, column];
        Field[row, column] = tempGO;
    }

    private void CountBorders()
    {
        int counterBoys = 0;
        int counterGirls = 0;
        int children = sadGirlsAndBoys.transform.childCount;
        for (int i = 0; i < children; ++i)
        {
            if (sadGirlsAndBoys.transform.GetChild(i).gameObject.activeSelf == true)
            {
                if (sadGirlsAndBoys.transform.GetChild(i).gameObject.tag == "Girl")
                    counterGirls++;
                else if (sadGirlsAndBoys.transform.GetChild(i).gameObject.tag == "Boy")
                    counterBoys++;

            }

        }
        sadGirlsCounter = counterGirls;
        sadBoysCounter = counterBoys;
        sadGirlsText.text = "" + sadGirlsCounter;
        sadBoysText.text = "" + sadBoysCounter;
    }

    public void RefreshText(int which)
    {
        if (which == 0)
            sadGirlsText.text = "" + sadGirlsCounter;
        else if (which == 1)
            sadBoysText.text = "" + sadBoysCounter;

        if (sadGirlsCounter == 0 && sadBoysCounter == 0)
            cannotContinue = true;
    }

    private void MoveUp()
    {
        for (int row = 0; row < FieldRowsNumber; row++)
        {
            for (int column = 0; column < FieldColumnsNumber; column++)
            {
                if (Field[row, column].tag == "Girl" || Field[row, column].tag == "Boy")
                {
                    int c = column;
                    for (int r = 0; r < row; r++)
                    {
                        if (Field[r, c].tag == "Empty")
                        {
                            Swap(r, c, row, column);
                        }
                    }
                }
            }
        }
    }
    
    private void MoveDown()
    {
        for (int row = FieldRowsNumber - 1; row >= 0; row--)
        {
            for (int column = 0; column < FieldColumnsNumber; column++)
            {
                if (Field[row, column].tag == "Girl" || Field[row, column].tag == "Boy")
                {
                    int c = column;
                    for (int r = FieldRowsNumber - 1; r >= row; r--)
                    {
                        if (Field[r, c].tag == "Empty")
                        {
                            Swap(r, c, row, column);
                        }
                    }
                }
            }
        }
    }

    private void MoveLeft()
    {
        for (int column = 0; column < FieldColumnsNumber; column++)
        {
            for (int row = 0; row < FieldRowsNumber; row++)
            {
                if (Field[row, column].tag == "Girl" || Field[row, column].tag == "Boy")
                {
                    int r = row;
                    for (int c = 0; c < column; c++)
                    {
                        if (Field[r, c].tag == "Empty")
                        {
                           Swap(r, c, row, column);
                        }
                    }
                }
            }
        }
    }

    private void MoveRight()
    {
        for (int column = FieldColumnsNumber - 1; column >= 0; column--)
        {
            for (int row = 0; row < FieldRowsNumber; row++)
            {
                if (Field[row, column].tag == "Girl" || Field[row, column].tag == "Boy")
                {
                    int r = row;
                    for (int c = FieldColumnsNumber - 1; c >= column; c--)
                    {
                        if (Field[r, c].tag == "Empty")
                        {
                            Swap(r, c, row, column);
                        }
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (cannotContinue == false)
        {
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                MoveUp();
                FertilizeChild();
                FertilizeChild();
                movesMade++;
                scoreText.text = "" + movesMade;
            }

            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                MoveDown();
                FertilizeChild();
                FertilizeChild();
                movesMade++;
                scoreText.text = "" + movesMade;
            }

            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                MoveLeft();
                FertilizeChild();
                FertilizeChild();
                movesMade++;
                scoreText.text = "" + movesMade;
            }

            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            {
                MoveRight();
                FertilizeChild();
                FertilizeChild();
                movesMade++;
                scoreText.text = "" + movesMade;
            }
        }
        else
        {
            if (isWin)
                YouWin.gameObject.SetActive(true);
            else
                YouLost.gameObject.SetActive(true);

            RestartButton.gameObject.SetActive(true);
            QuitButton.gameObject.SetActive(true);

            GameGrid.SetActive(false);
        }
    }
}
