﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KafelekBoyController : MonoBehaviour
{
    public Animator animator;
    private float counter;
   
    // Start is called before the first frame update
    void Start()
    {
        counter = Random.Range(0.0f, 1.0f);
        animator = this.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime;
        
        if (counter > 1.5f)
        {
            animator.SetTrigger("move_eyes");
            counter = 0.0f;
        }
    }
}
