﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KafelekGirlController : MonoBehaviour
{
    // Start is called before the first frame update
   public Animator animator;
   private float counter;
   
   // Start is called before the first frame update
   void Start()
   {
       counter = Random.Range(0.0f, 1.0f);
       animator = this.gameObject.GetComponent<Animator>();
   }

   // Update is called once per frame
   void Update()
   {
       counter += Time.deltaTime;

       int which = Random.Range(0, 3);
       if (counter > 1.5f)
       {
           if (which == 0)
           {
               animator.SetTrigger("move_eyes");
           }
           else if (which == 1)
           {
               animator.SetTrigger("move_eyes_2");
           }
           else
           {
               animator.SetTrigger("move_eyes_3");
           }

           counter = 0.0f;
       }
   }
}
