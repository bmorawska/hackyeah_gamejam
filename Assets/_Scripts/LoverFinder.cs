﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoverFinder : MonoBehaviour
{
    public Sprite EmptyKafelekSprite; 
    public Animator animator;

    public GameObject EmptyPrefab;
    public bool isPaired = false;
    // Start is called before the first frame update
    void Start()
    {
        animator = this.gameObject.GetComponent<Animator>();
    }
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        if ((col.gameObject.GetComponent<Rigidbody2D>() == null) && 
            (col.gameObject.tag != "Empty") &&
            (col.gameObject.tag != this.gameObject.tag) &&
            (isPaired == false))
        {
            col.gameObject.GetComponent<SpriteRenderer>().sprite = EmptyKafelekSprite;
            if (col.gameObject.tag == "Girl")
            {
                Destroy(col.gameObject.GetComponent<KafelekGirlController>());
            }
            else
            {
                Destroy(col.gameObject.GetComponent<KafelekBoyController>());
            }
            col.gameObject.tag = "Empty";

            Destroy(col.gameObject.GetComponent<Animator>());
            
            if (this.gameObject.tag == "Girl")
            {
                col.gameObject.GetComponentInParent<PlayerController>().sadGirlsCounter--;
                col.gameObject.GetComponentInParent<PlayerController>().RefreshText(0);
            } 
            else
            {
                col.gameObject.GetComponentInParent<PlayerController>().sadBoysCounter--;
                col.gameObject.GetComponentInParent<PlayerController>().RefreshText(1);
            }
            isPaired = true;
            animator.SetTrigger("start_loving");
            AudioManager.instance.Play("Love");
            StartCoroutine(WaitForAnimation());
        }
    }
    
    private IEnumerator WaitForAnimation()
    {
        yield return new WaitForSeconds(1.0f);
        this.gameObject.SetActive(false);
    }
}
